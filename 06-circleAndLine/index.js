/** @type {HTMLCanvasElement} */
const canvas = document.querySelector('#canvas')
const width = window.innerWidth
const height = window.innerHeight
canvas.width = width
canvas.height = height
const ctx = canvas.getContext('2d')

const tailStyle = {
    len:10,
    startColor : '#d63031',
    endColor:'#fd79a8'
}

const circleNum = 4

const colors = ['#55efc4','#0984e3','#d63031','#e67e22']

function Circle(x, y, r, vx, vy, fillStyle) {
    this.x = x
    this.y = y
    this.r = r
    this.vx = vx
    this.vy = vy
    this.fillStyle = fillStyle
    this.tailPath = []

    this.draw = function () {
        ctx.beginPath()
        ctx.lineWidth = 0
        ctx.arc(this.x, this.y, this.r, 0, Math.PI * 2, false)
        ctx.fillStyle = this.fillStyle
        ctx.fill()
        this.drawTail()
    }

    this.drawTail = function(){
        ctx.lineWidth = this.r
        // ctx.lineCap = 'round'
        ctx.lineJoin = 'round'

        //渐变style
        const end = this.tailPath[0]
        const start = this.tailPath[this.tailPath.length-1]
        const grd=ctx.createLinearGradient(...start,...end);
        grd.addColorStop(0,this.fillStyle);
        grd.addColorStop(0.5,tailStyle.endColor)
        grd.addColorStop(1,'#fff')
        ctx.strokeStyle = grd

        ctx.shadowColor = '#81ecec'
        ctx.shadowBlur = 16

        ctx.beginPath()
        const path = [...this.tailPath]
        ctx.moveTo(...path.shift())
        while(path.length>0){
            ctx.lineTo(...path.shift())
            ctx.stroke();
        }
    }

    this.update = function () {
        const p = [this.x,this.y]
        while(this.tailPath.length>tailStyle.len){
            this.tailPath.shift()
        }
        this.tailPath.push(p)
        this.x += this.vx
        this.y += this.vy

        //碰壁反弹
        if (this.x - this.r <= 0 || this.x + this.r >= width) {
            this.vx *= -1
        }
        if (this.y - this.r <= 0 || this.y + this.r >= height) {
            this.vy *= -1
        }
    }
}


function randomPos(){
    const x = Math.floor(Math.random()*(width-200)+100)
    const y = Math.floor(Math.random()*(height-200)+100)
    return [x,y]
}

function randomColor(){
    const len = colors.length
    return colors[Math.floor(Math.random()*len)]
}

const circleArr = []

function init(){
    for(let i = 0;i<circleNum;i++){
        const circle = new Circle(...randomPos(), 10, 10, 10, randomColor())
        circleArr.push(circle)
    }
}

init()

function animate() {
    window.requestAnimationFrame(animate)
    ctx.clearRect(0, 0, width, height)

    circleArr.forEach(c=>{
        c.update()
        c.draw()
    })
}

animate()