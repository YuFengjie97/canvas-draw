/** @type {HTMLCanvasElement} */
const canvas = document.querySelector('#canvas')
const width = window.innerWidth
const height = window.innerHeight
canvas.width = width
canvas.height = height
const ctx = canvas.getContext('2d')

let imageData

//载入图片
const image = new Image()
image.src = '../images/dragon.png'

image.onload = function () {

    const imageWidth = image.width
    const imageHeight = image.height

    const coverWidth = width
    const coverHeight = width/imageWidth*imageHeight

    canvas.width = coverWidth
    canvas.height = coverHeight

    console.log([coverWidth,coverHeight]);

    //以宽为基准
    ctx.drawImage(image, 0, 0, coverWidth,coverHeight);


    //获取图片点阵数据
    imageData = ctx.getImageData(0, 0, coverWidth, coverHeight).data
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    // ctx.fillRect(0,0, canvas.width, canvas.height);


    //点阵数据绘制
    ctx.fillStyle = '#fff'
    const gap = 6
    for (let h = 0; h < coverWidth; h += gap) {
        for (let w = 0; w < coverHeight; w += gap) {
            const position = (coverWidth * h + w) * 6
            const r = imageData[position], g = imageData[position + 1], b = imageData[position + 2];
            if (r + g + b == 0) {
                ctx.fillStyle = "#000"
                ctx.fillRect(w, h, 4, 4);
            }
        }
    }
}




