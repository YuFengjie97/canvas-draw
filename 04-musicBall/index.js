/** @type {HTMLCanvasElement} */

const canvas = document.querySelector('#canvas')
let WIDTH = window.innerWidth
let HEIGHT = window.innerHeight
canvas.width = WIDTH
canvas.height = HEIGHT
const ctx = canvas.getContext('2d')

const mouse = {
    x: undefined,
    y: undefined
}

//canvas相关
const colors = ['#d63031', '#0984e3', '#55efc4']
const rMax = 40
const speedBase = 3

//音频相关
const fftSize = 512
let analyser = null
let dataArray
const audio = document.querySelector('audio')

function Circle(x, y, r, fillStyle, vx, vy) {
    this.x = x
    this.y = y
    this.r = r
    this.rDefault = r
    this.fillStyle = fillStyle
    this.vx = vx
    this.vy = vy

    this.draw = function () {
        ctx.beginPath()
        ctx.lineWidth = 0
        ctx.arc(this.x, this.y, this.r, 0, Math.PI * 2, false)

        ctx.fillStyle = this.fillStyle
        ctx.fill()
        // ctx.strokeStyle = 'transparent'
        // ctx.stroke()

        ctx.shadowColor = fillStyle
        ctx.shadowBlur = 20
    }

    this.update = function () {
        this.x += this.vx
        this.y += this.vy

        //碰壁反弹
        if (this.x - this.r <= 0 || this.x + this.r >= WIDTH) {
            this.vx *= -1
        }
        if (this.y - this.r <= 0 || this.y + this.r >= HEIGHT) {
            this.vy *= -1
        }
    }
}

const circleArr = []

function init() {
    circleArr.length = 0
    for (let i = 0; i < fftSize / 2; i++) {
        const r = randomNum(0, 10)
        const x = randomNum(0, WIDTH - 2 * r)
        const y = randomNum(0, HEIGHT - 2 * r)
        const fill = colors[randomNum(0, colors.length)]
        const vx = Math.random() * speedBase * (Math.random() - 0.5 > 0 ? 1 : -1)
        const vy = Math.random() * speedBase * (Math.random() - 0.5 > 0 ? 1 : -1)
        const circle = new Circle(x, y, r, fill, vx, vy)
        circleArr.push(circle)
    }
}
init()

function randomNum(min, max) {
    return Math.floor(Math.random() * (max - min) + min)
}


function animate() {
    window.requestAnimationFrame(animate)
    ctx.clearRect(0, 0, WIDTH, HEIGHT)

    analyser.getByteFrequencyData(dataArray)

    circleArr.forEach((circle, index) => {
        let r = Math.ceil(dataArray[index] * 0.2)
        const v = Math.ceil(dataArray[index] * 0.01)
        circle.r = r
        circle.vx = v*(Math.random()-0.5>0?1:-1)
        circle.vy = v*(Math.random()-0.5>0?1:-1)

        circle.update()
        circle.draw()
    })
}

//---------------------华丽的分界线------------------------------------------

function startMusic() {
    let bt = document.querySelector('.start')
    bt.setAttribute('class', 'start disappear')
    audio.play()
    musicLoad()
    animate()
}

function musicLoad() {
    const audioCtx = new (window.AudioContext || window.webkitAudioContext)()

    analyser = audioCtx.createAnalyser()
    analyser.fftSize = fftSize

    const source = audioCtx.createMediaElementSource(audio)
    source.connect(analyser)
    analyser.connect(audioCtx.destination)

    const bufferLength = analyser.frequencyBinCount
    dataArray = new Uint8Array(bufferLength)
}
