/** @type {HTMLCanvasElement} */

const canvas = document.querySelector('#canvas')
let WIDTH = window.innerWidth - 10
let HEIGHT = window.innerHeight - 10
canvas.width = WIDTH
canvas.height = HEIGHT
const ctx = canvas.getContext('2d')

const mouse = {
    x: undefined,
    y: undefined
}

const colors = ['#d63031', '#0984e3', '#55efc4']
const speedBase = 3
const circleSum = 100

const gravity = 1
const friction = 0.8//垂直阻力抵消后剩余百分比
const friction2 = 0.16//水平阻力抵消后剩余百分比


function Circle(x, y, r, fillStyle, dx, dy) {
    this.x = x
    this.y = y
    this.r = r
    this.rDefault = r
    this.fillStyle = fillStyle
    this.dx = dx
    this.dy = dy

    this.draw = function () {
        ctx.beginPath()
        ctx.lineWidth = 0
        ctx.arc(this.x, this.y, this.r, 0, Math.PI * 2, false)

        ctx.fillStyle = this.fillStyle
        ctx.fill()
        // ctx.strokeStyle = 'transparent'
        // ctx.stroke()

        ctx.shadowColor = fillStyle
        ctx.shadowBlur = 20
    }

    this.update = function () {
        if (this.y - this.r < 0 || this.y + this.r > HEIGHT) {
            // if (Math.abs(this.dy) < 0.1) {
            //     this.dy = 0
            // }
            this.dy = -this.dy * friction
        } else {
            this.dy += gravity
        }
        if (this.x - this.r < 0 || this.x + this.r > WIDTH) {
            this.dx = -this.dx * friction2
        }

        this.x += this.dx
        this.y += this.dy
    }
}

const circleArr = []

function init() {
    circleArr.length = 0
    for (let i = 0; i < circleSum; i++) {
        const r = randomNum(0, 20)
        const x = randomNum(r, WIDTH - r)
        const y = randomNum(r, HEIGHT - r)
        const fill = colors[randomNum(0, colors.length)]
        const dx = Math.random() * speedBase * (Math.random() - 0.5 > 0 ? 1 : -1)
        const dy = Math.random() * speedBase * (Math.random() - 0.5 > 0 ? 1 : -1)
        const circle = new Circle(x, y, r, fill, dx, dy)
        circleArr.push(circle)
    }
}
init()
animate()

function randomNum(min, max) {
    return Math.floor(Math.random() * (max - min) + min)
}


function animate() {
    window.requestAnimationFrame(animate)
    ctx.clearRect(0, 0, WIDTH, HEIGHT)

    circleArr.forEach((circle) => {
        circle.update()
        circle.draw()
    })
}

