# canvasDraw

> 因为部分html的script标签使用了type=“module"属性,所以,当你直接打开时,控制台会提示跨域错误信息  
> 你需要起一个服务,推荐使用vscode的Live Server插件

## 01-draw
> 画画

## 02-bounceBall
> 不停运动的小球

## 03-mouseEvent
> 在上面的基础上,添加鼠标交互

## 04-musicBall
> 在上面的基础上,小球与音乐联动

## 05-gravity
> 小球模拟重力,摩擦力

## 06-circleAndLine
> 一个圆一条尾部的线,添加渐变style,我是一条青儿虫,你是一条猪儿虫,每次更新tailpath的数组来重新绘制实在是太蠢了
## 06-circleAndLine2
> 改进版,用fillReact来制作蒙版

## 07-dragon
> 载入图片,获取点阵,画一条龙,跟着我左手画一条龙,右手画一条彩虹

## 08-text
> 文字
## 09-circularMotion
> 粒子旋转特效
- mouse,鼠标位置
- particleNum粒子数量
- 以下部分为魔法
- radians,弧度
- distanceFromCenter,粒子距离中心距离
- lastMouse,拖拽时效果,不是直接移动
- this.draw()屏蔽画圆,只保留画线
- lastPoint,线尾
- this.update(),这是魔法,我没法跟你解释
- animate(),clearRect改为fillRect,并且透明色为0.05,想象不停渲染,铺了一层又一层的透明纸

## 010-cirAndLine
> 经典效果,有许多点,小于一定距离,会绘制线连接,大于会断开,断开时,渐隐效果没实现

## 010-cirAndLine2
> 经典效果反色

## 011-particle
> 在09点基础上,梳理,stats.js监控,控制
- stats帧率监控
- gui可视化控制
- 基础
    - 粒子个数
    - 环绕最小半径
    - 环绕最大半径
    - 粒子半径
    - 是否绘制circle
- 魔法
    - 向心力速度
    - x轴变换函数
    - y轴变换函数

## 012-mouseRect
> canvasg更新坐标和css更新坐标,哪个比较流畅?

## 013-translate&rotate
> 我看不懂,但我大受震撼

## 014-codeRain
> 代码雨

## 015-p5
> p5的一个例子,amaazing,这什么神仙代码