/** @type {HTMLCanvasElement} */

const canvas = document.querySelector('#canvas')
const width = window.innerWidth
const height = window.innerHeight
canvas.width = width
canvas.height = height
const ctx = canvas.getContext('2d')

const mouse = {
    x:0,
    y:0
}

canvas.addEventListener('mousemove',function(e){
    mouse.x = e.x
    mouse.y = e.y
})

function drawRect(){
    ctx.fillStyle = 'red'
    ctx.fillRect(mouse.x,mouse.y,100,100)
}

function animate(){
    window.requestAnimationFrame(animate)
    ctx.clearRect(0,0,width,height)
    drawRect()
}

animate()