window.addEventListener('load', function () {
    const canvas = document.querySelector('#canvas')
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight
    const ctx = canvas.getContext('2d')

    let isPainting = false

    function startDraw(e) {
        isPainting = true
        ctx.moveTo(e.clientX, e.clientY)
        ctx.lineWidth = 4
        ctx.lineCap = 'round'
        // ctx.lineJoin = 'round'

        ctx.strokeStyle = '#ff7675'
        ctx.shadowColor = '#a29bfe'
        ctx.shadowBlur = 4
    }

    function endDraw() {
        isPainting = false
    }

    function draw(e) {
        if (isPainting) {
            ctx.lineTo(e.clientX, e.clientY)
            ctx.stroke();
        }
    }

    canvas.addEventListener('mousedown', startDraw)
    canvas.addEventListener('mouseup', endDraw)
    canvas.addEventListener('mousemove', draw)

})

