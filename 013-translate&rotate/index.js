/** @type {HTMLCanvasElement} */

const canvas = document.querySelector('#canvas')
const width = window.innerWidth
const height = window.innerHeight
canvas.width = width
canvas.height = height
const ctx = canvas.getContext('2d')

const centerX = width/2
const centerY = height/2

let rotateDeg = 10

function draw(){
    ctx.beginPath()
    ctx.translate(centerX,centerY)
    ctx.rotate(rotateDeg)
    ctx.fillStyle = 'red'
    ctx.fillRect(centerX, centerY, 100, 100)
}

function update(){
    rotateDeg += 0.00001
    draw()
}

function animate(){
    ctx.clearRect(0,0,width,height)
    window.requestAnimationFrame(animate)
    update()
}

animate()