/** @type {HTMLCanvasElement} */

const canvas = document.querySelector('#canvas')
let WIDTH = window.innerWidth
let HEIGHT = window.innerHeight
canvas.width = WIDTH
canvas.height = HEIGHT
const ctx = canvas.getContext('2d')

const mouse = {
    x: undefined,
    y: undefined
}
const colors = ['#2C3E50', '#E74C3C', '#ECF0F1', '#3498DB', '#2980B9']
const rMax = 40

window.addEventListener('resize', function () {
    WIDTH = window.innerWidth
    HEIGHT = window.innerHeight
    canvas.width = WIDTH
    canvas.height = HEIGHT
    init()
})

canvas.addEventListener('mousemove', function (e) {
    mouse.x = e.x
    mouse.y = e.y
})

function Circle(x, y, r, fillStyle, vx, vy) {
    this.x = x
    this.y = y
    this.r = r
    this.rDefault = r
    this.fillStyle = fillStyle
    this.vx = vx
    this.vy = vy

    this.draw = function () {
        ctx.beginPath()
        ctx.lineWidth = 0
        ctx.arc(this.x, this.y, this.r, 0, Math.PI * 2, false)

        ctx.fillStyle = this.fillStyle
        ctx.fill()
        ctx.strokeStyle = 'transparent'
        ctx.stroke()
    }

    this.update = function () {
        //不断运动
        this.x += this.vx
        this.y += this.vy

        //碰壁反弹
        if (this.x - this.r <= 0 || this.x + this.r >= WIDTH) {
            this.vx *= -1
        }
        if (this.y - this.r <= 0 || this.y + this.r >= HEIGHT) {
            this.vy *= -1
        }

        //鼠标划上放大
        let zone = 50
        if (mouse.x - zone < this.x && this.x < mouse.x + zone && mouse.y - zone < this.y && this.y < mouse.y + zone) {
            if (this.r < rMax) this.r += 3
        } else if (this.r > this.rDefault) {
            this.r -= 1
        }
    }
}

const circleArr = []

function init() {
    circleArr.length = 0
    for (let i = 0; i < 800; i++) {
        const r = randomNum(0, 10)
        const x = randomNum(0, WIDTH - 2 * r)
        const y = randomNum(0, HEIGHT - 2 * r)
        const fill = colors[randomNum(0, colors.length)]
        const vx = Math.random() * 2 * (Math.random() - 0.5 > 0 ? 1 : -1)
        const vy = Math.random() * 2 * (Math.random() - 0.5 > 0 ? 1 : -1)
        const circle = new Circle(x, y, r, fill, vx, vy)
        circleArr.push(circle)
    }
}
init()

function randomNum(min, max) {
    return Math.floor(Math.random() * (max - min) + min)
}


function animate() {
    window.requestAnimationFrame(animate)
    ctx.clearRect(0, 0, WIDTH, HEIGHT)

    circleArr.forEach(c => {
        c.update()
        c.draw()
    })
}
animate()