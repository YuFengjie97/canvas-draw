/** @type {HTMLCanvasElement} */

const canvas = document.querySelector('#canvas')
let width = window.innerWidth
let height = window.innerHeight
canvas.width = width
canvas.height = height
const ctx = canvas.getContext('2d')

const mouse = {
    x: 0,
    y: 0
}
const colors = ['#d63031', '#0984e3', '#55efc4']
const particleNum = 10

canvas.addEventListener('mousemove',function(e){
    mouse.x = e.x
    mouse.y = e.y
})


function Particle(x, y, radius, color) {
    this.x = x
    this.y = y
    this.radius = radius
    this.color = color

    //以下部分为魔法
    this.radians = Math.random() * Math.PI * 2
    this.velocity = 0.05//改变速率会有不同的效果,amiazing
    //转起来像3D的
    // this.distanceFromCenter = {
    //     x:randomNum(50,120),
    //     y:randomNum(50,120),
    // }
    //转起来像2D
    this.distanceFromCenter = randomNum(50, 120)
    this.lastMouse = {
        x:x,
        y:y
    }

    this.draw = function (lastPoint) {
        //画圆
        // ctx.beginPath()
        // ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
        // ctx.fillStyle = this.color
        // ctx.fill()

        //画tail
        ctx.beginPath()
        ctx.strokeStyle = this.color
        ctx.lineWidth = this.radius
        ctx.moveTo(lastPoint.x, lastPoint.y)
        ctx.lineTo(this.x, this.y)
        ctx.stroke()
        ctx.closePath()
    }

    this.update = function () {
        const lastPoint = { x: this.x, y: this.y }

        this.lastMouse.x += (mouse.x - this.lastMouse.x) * 0.05;
        this.lastMouse.y += (mouse.y - this.lastMouse.y) * 0.05;

        this.radians += this.velocity
        // this.x = x + Math.cos(this.radians)*this.distanceFromCenter.x
        // this.y = y + Math.sin(this.radians)*this.distanceFromCenter.y
        this.x = this.lastMouse.x + Math.cos(this.radians) * this.distanceFromCenter
        this.y = this.lastMouse.y + Math.sin(this.radians) * this.distanceFromCenter

        this.draw(lastPoint)
    }
}

const particleArr = []

function init() {
    for (let i = 0; i < particleNum; i++) {
        const radius = 4
        const x = width / 2
        const y = height / 2
        const color = colors[randomNum(0, colors.length)]
        const particle = new Particle(x, y, radius, color)
        particleArr.push(particle)
    }
}

function randomNum(min, max) {
    return Math.floor(Math.random() * (max - min) + min)
}

function animate() {
    window.requestAnimationFrame(animate)
    //以下为魔法部分
    ctx.fillStyle = 'rgba(0,0,0,0.05)'
    ctx.fillRect(0, 0, width, height)
    // ctx.clearRect(0, 0, width, height)

    particleArr.forEach((item) => {
        item.update()
    })
}

init()
animate()
