/** @type {HTMLCanvasElement} */

const canvas = document.querySelector('#canvas')
const WIDTH = window.innerWidth
const HEIGHT = window.innerHeight
canvas.width = WIDTH
canvas.height = HEIGHT
const ctx = canvas.getContext('2d')


function Circle(x, y, r, fillStyle, vx, vy) {
    this.x = x
    this.y = y
    this.r = r
    this.fillStyle = fillStyle
    this.vx = vx
    this.vy = vy

    this.draw = function () {
        ctx.beginPath()
        ctx.lineWidth = 0
        ctx.arc(this.x, this.y, this.r, 0, Math.PI * 2, false)

        ctx.fillStyle = this.fillStyle
        ctx.fill()
        ctx.strokeStyle = 'transparent'
        ctx.stroke()
    }

    this.update = function () {
        this.x += this.vx
        this.y += this.vy

        if (this.x - this.r <= 0 || this.x + this.r >= WIDTH) {
            this.vx *= -1
        }
        if (this.y - this.r <= 0 || this.y + this.r >= HEIGHT) {
            this.vy *= -1
        }
    }
}

const circleArr = []
for (let i = 0; i < 200; i++) {
    const r = Math.random() * 30
    const x = Math.random() * (WIDTH - 2 * r) + r
    const y = Math.random() * (HEIGHT - 2 * r) + r
    const fill = randomColor()
    const vx = Math.random() * 8 * (Math.random() - 0.5 > 0 ? 1 : -1)
    const vy = Math.random() * 8 * (Math.random() - 0.5 > 0 ? 1 : -1)
    const circle = new Circle(x, y, r, fill, vx, vy)
    circleArr.push(circle)
}

function randomColor() {
    const r = Math.floor(Math.random() * 255)
    const g = Math.floor(Math.random() * 255)
    const b = Math.floor(Math.random() * 255)
    return `rgba(${r},${g},${b},0.7)`
}


function animate() {
    window.requestAnimationFrame(animate)
    ctx.clearRect(0, 0, WIDTH, HEIGHT)

    circleArr.forEach(c => {
        c.update()
        c.draw()
    })
}

animate()
