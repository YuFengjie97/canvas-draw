/** @type {HTMLCanvasElement} */
const canvas = document.querySelector('#canvas')
const width = window.innerWidth
const height = window.innerHeight
canvas.width = width
canvas.height = height
const ctx = canvas.getContext('2d')

const text = 'hello world'

const mouse = {
    x:0,
    y:0
}

canvas.addEventListener('mousemove',function(e){
    mouse.x = e.x
    mouse.y = e.y
})

function draw(){
    ctx.font = "30px Arial"
    //实心
    // ctx.fillStyle = 'red'
    // ctx.fillText(text,mouse.x,mouse.y)

    //空心
    ctx.strokeStyle = 'red'
    ctx.strokeText(text,mouse.x,mouse.y)
}

function update(){
    window.requestAnimationFrame(update)
    // ctx.clearRect(0,0,width,height)
    draw()
}

update()